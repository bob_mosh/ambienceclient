//
//  SoundType.swift
//  Ambience
//
//  Created by Ferdinand Göldner on 18.05.19.
//  Copyright © 2019 Ferdinand Göldner. All rights reserved.
//

import UIKit

struct SoundType {
    static let music                    = UIColor(red: 0.00, green: 0.58, blue: 0.81, alpha: 1.00)
    static let ambientSound             = UIColor(red: 0.99, green: 0.81, blue: 0.33, alpha: 1.00)
    static let ambientSoundFaded        = UIColor(red: 0.99, green: 0.81, blue: 0.33, alpha: 0.30)
    static let ambientSoundPreset       = UIColor(red: 0.26, green: 0.58, blue: 0.31, alpha: 1.00)
    static let ambientSoundPresetFaded  = UIColor(red: 0.26, green: 0.58, blue: 0.31, alpha: 0.30)
    static let soundEffect              = UIColor(red: 0.71, green: 0.14, blue: 0.22, alpha: 1.00)
    static let transparent              = UIColor(red: 0.00, green: 0.00, blue: 0.00, alpha: 0.05)
}
