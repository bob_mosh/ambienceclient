//
//  ViewController.swift
//  AmbienceClient
//
//  Created by Ferdinand Göldner on 18.05.19.
//  Copyright © 2019 Ferdinand Göldner. All rights reserved.
//

import UIKit
import FirebaseDatabase

class ViewController: UIViewController {
    var sounds: [String: [SoundDataset]] = [:]
    var databaseRef: DatabaseReference? = nil
    
    @IBOutlet weak var volumeSlider: UISlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        databaseRef = Database.database().reference()
        
        // Do any additional setup after loading the view.
        // Populate the table with the datasets
        //        sounds = SoundLibrary().ambientSounds
        //
        //        for environment in sounds {
        //            for sound in environment.value {
        //                let soundName = "\(sound.sound!)".split(separator: ".").last ?? ""
        //                debugPrint(soundName)
        //                databaseRef?.child("session1/\(soundName)").setValue(0)
        //            }
        //        }
        
        
        
        let ref = Database.database().reference()
        ref.child("session1").observeSingleEvent(of: .value) { (snapshot) in
            let table = (snapshot.value as? [String: Int]) ?? [:]
            for dataSet in (table) {
                ref.child("session1/" + dataSet.key).observe(.value) { (snapshot) in
                    debugPrint(snapshot.key + ":  \(snapshot.value as? Int ?? 0)")
                }
            }

        }
        
        
        
        //        let value = ref.child("session1").observe(.value) { (snapshot) in
        //            let data = snapshot.value as? [String: Int]
        //            for dataSet in (data ?? [:]) {
        //                debugPrint(dataSet.key + ": " + String(dataSet.value))
        //            }
        //        }
    }
    
    @IBAction func volumeChanged(_ sender: Any) {
    }
    
}

